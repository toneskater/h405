/*
 * cpum.h
 *
 *  Created on: 10/set/2014
 *      Author: Andrea Tonello
 */

#ifndef CPUM_H_
#define CPUM_H_

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "common_conf.h"
   
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define CPUM_TICKS_MS (1000)
//#define CPUM_TWO_COUNTER
//#define CPUM_DISABLE_THREADS
   
#ifdef CPUM_TWO_COUNTER 
#define CPUM_INCREMENT_WITHOUT_THREAD       (0x01002D96) 
#else
#define CPUM_INCREMENT_WITHOUT_THREAD       (0x006287A2)  
#endif
/* Public define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/
void CPUM_Init(void);

#ifdef __cplusplus
}
#endif

#endif /* CPUM_H_ */
