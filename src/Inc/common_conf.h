/*
 * common_conf.h
 *
 *  Created on: 10/set/2014
 *      Author: Andrea Tonello
 */

#ifndef COMMON_CONF_H_
#define COMMON_CONF_H_

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
   
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
      
/* Private typedef -----------------------------------------------------------*/

typedef enum SYST_tids_e
{
  SYST_HANDLE_CPUM = 0,
  SYST_HANDLE_MAX_NUMBER
}SYST_tids_t;

typedef uint8_t  ubyte;
typedef uint16_t ushort;
typedef uint32_t ulong;
   
typedef char  sbyte;
typedef int16_t sshort;
typedef int32_t slong;

#define SHORT_MASK_BYTE1(x) ((x & 0xFF00) >> 8)
#define SHORT_MASK_BYTE0(x) (x & 0x00FF)
#define LONG_MASK_BYTE3(x) ((x & 0xFF000000) >> 24)
#define LONG_MASK_BYTE2(x) ((x & 0x00FF0000) >> 16)
#define LONG_MASK_BYTE1(x) ((x & 0x0000FF00) >> 8)
#define LONG_MASK_BYTE0(x) ((x & 0x000000FF))

#define BIT_TO_MASK(x) (1 << x)

typedef union bitfield8_u{
  struct{
#ifdef NOT_USED
    ubyte bit7 : 1;
    ubyte bit6 : 1;
    ubyte bit5 : 1;
    ubyte bit4 : 1;
    ubyte bit3 : 1;
    ubyte bit2 : 1;
    ubyte bit1 : 1;
    ubyte bit0 : 1;
#endif
    ubyte bit0 : 1;
    ubyte bit1 : 1;
    ubyte bit2 : 1;
    ubyte bit3 : 1;
    ubyte bit4 : 1;
    ubyte bit5 : 1;
    ubyte bit6 : 1;
    ubyte bit7 : 1;
  }bit_t;
  ubyte bits;
}bitfield8_t;

typedef enum SYST_RESET_TYPE_e
{
  SYST_RESET_TYPE_POWER_ON = 0,
  SYST_RESET_TYPE_MEMMANAGE_FAULT,
  SYST_RESET_TYPE_BUS_FAULT,
  SYST_RESET_TYPE_USAGE_FAULT,
  SYST_RESET_TYPE_HARD_FAULT,
  SYST_RESET_TYPE_ERROR_HANDLER,
  SYST_RESET_TYPE_ASSERT_FAILED,
  SYST_RESET_TYPE_STACK_OVERFLOW,
  SYST_RESET_TYPE_MAX_NUMBER
}SYST_RESET_TYPE_t;

/* Private define ------------------------------------------------------------*/

/* Public define ------------------------------------------------------------*/
#define SYST_LITTLE_ENDIAN
#define FALSE 0U
#define TRUE 1U
#define portMAX_DELAY ( TickType_t ) 0xffffffffUL

/* #define HART_ENABLE_FUNCTION */

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/
extern TaskHandle_t SYST_tids[SYST_HANDLE_MAX_NUMBER];
extern const ubyte SYST_tpriority[SYST_HANDLE_MAX_NUMBER];
extern const ushort SYST_taskStackSize[SYST_HANDLE_MAX_NUMBER];
extern const ubyte SYST_taskEnable[SYST_HANDLE_MAX_NUMBER];
extern float SYST_CpuLoadPercentage;
extern SYST_tids_t overflowedTask;
extern SYST_RESET_TYPE_t SYST_ResetType;
extern SYST_RESET_TYPE_t SYST_ResetType;

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/
/* extern void Error_Handler(void); */
void SYST_SetResetType(SYST_RESET_TYPE_t type);

#ifdef __cplusplus
}
#endif

#endif /* COMMON_CONF_H_ */
