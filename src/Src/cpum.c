/*
 * cpum.c
 *
 *  Created on: 08/gen/2014
 *      Author: Andrea Tonello
 */

/* Includes ------------------------------------------------------------------*/
#include "cpum.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static ulong cpuCounter = 0;
static ulong lastCpuCounter = 0;
static TickType_t Cpum_xLastExecutionTime;

/* Public variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
static void vCpumTask( void *pvParameters );
#ifdef CPUM_TWO_COUNTER
static void vCpucTask( void *pvParameters );
#endif

/* Public functions ----------------------------------------------------------*/

void CPUM_Init(void)
{
  GPIO_InitTypeDef   GPIO_InitStructure;
  if(SYST_taskEnable[SYST_HANDLE_CPUM] != FALSE)
  {
    if(xTaskCreate( vCpumTask, "CPUM", SYST_taskStackSize[SYST_HANDLE_CPUM], NULL, SYST_tpriority[SYST_HANDLE_CPUM], &SYST_tids[SYST_HANDLE_CPUM] ) == pdPASS)
    {
      configASSERT( SYST_tids[SYST_HANDLE_CPUM] );
    }
    
    /* Enable the port of led */
    __GPIOC_CLK_ENABLE();

    GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStructure.Pull = GPIO_PULLDOWN;
    GPIO_InitStructure.Pin = GPIO_PIN_12;
    GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);
#ifndef CPUM_TWO_COUNTER     
    cpuCounter = 0;
    lastCpuCounter = 0;
    Cpum_xLastExecutionTime = 0;
#endif
  }
  
#ifdef CPUM_TWO_COUNTER  
  if(SYST_taskEnable[SYST_HANDLE_CPUC] != FALSE)
  {
    if(xTaskCreate( vCpucTask, "CPUC", SYST_taskStackSize[SYST_HANDLE_CPUC], NULL, SYST_tpriority[SYST_HANDLE_CPUC], &SYST_tids[SYST_HANDLE_CPUC] ) == pdPASS)
    {
      configASSERT( SYST_tids[SYST_HANDLE_CPUC] );
    }
    
    cpuCounter = 0;
    lastCpuCounter = 0;
  }
#endif
}

#ifdef CPUM_TWO_COUNTER 
static void vCpucTask( void *pvParameters )
{
  while(TRUE)
  {
    ++cpuCounter;
  }
}
#endif

static void vCpumTask( void *pvParameters )
{
#ifndef CPUM_TWO_COUNTER 
  ulong tick;
#else
  Cpum_xLastExecutionTime = xTaskGetTickCount();
#endif  
  while(TRUE)
  {
#ifndef CPUM_TWO_COUNTER 
    ++cpuCounter;
    
    tick = xTaskGetTickCount();
    
    if((tick - Cpum_xLastExecutionTime) > (pdMS_TO_TICKS(CPUM_TICKS_MS) - 1))
#else
    vTaskDelayUntil(&Cpum_xLastExecutionTime, pdMS_TO_TICKS(CPUM_TICKS_MS));
#endif
    {
#ifndef CPUM_TWO_COUNTER 
      Cpum_xLastExecutionTime = tick;
#endif
      lastCpuCounter = cpuCounter;
#ifdef CPUM_INCREMENT_WITHOUT_THREAD
      if(lastCpuCounter > CPUM_INCREMENT_WITHOUT_THREAD)
      {
        lastCpuCounter = CPUM_INCREMENT_WITHOUT_THREAD;
      }
#endif
      cpuCounter = 0;

      GPIOC->ODR ^= GPIO_PIN_12;

#ifdef CPUM_INCREMENT_WITHOUT_THREAD
      SYST_CpuLoadPercentage = ((((float)((CPUM_INCREMENT_WITHOUT_THREAD - lastCpuCounter) * 0x00000064)) / ((float)CPUM_INCREMENT_WITHOUT_THREAD))/* * 100.0f*/);
      /*
      if(SYST_CpuLoadPercentage > EEPM_Ram_SYST_CPU_MAX_LOAD)
      {
        EEPM_WriteInEeprom(EEPM_ID_SYST_CPU_MAX_LOAD, (void*)&SYST_CpuLoadPercentage, sizeof(SYST_CpuLoadPercentage));
      }
      */
#endif
    }
  }
}
