/*
 * common_conf.c
 *
 *  Created on: 10/set/2014
 *      Author: Andrea Tonello
 */

/* Includes ------------------------------------------------------------------*/
#include "common_conf.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Public define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Public variables ----------------------------------------------------------*/

SYST_tids_t overflowedTask;

TaskHandle_t SYST_tids[SYST_HANDLE_MAX_NUMBER];

const ubyte SYST_tpriority[SYST_HANDLE_MAX_NUMBER] = 
{
  /* SYST_HANDLE_CPUM = 0,   */ ( tskIDLE_PRIORITY +  2 ),
};  

const ushort SYST_taskStackSize[SYST_HANDLE_MAX_NUMBER] = 
{
  /* SYST_HANDLE_CPUM,       */ ( 100                      +   0 ),
};  

const ubyte SYST_taskEnable[SYST_HANDLE_MAX_NUMBER] = 
{
  /* SYST_HANDLE_CPUM,       */ ( TRUE  ),
};

float SYST_CpuLoadPercentage = 0.0f;
SYST_RESET_TYPE_t SYST_ResetType;

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/


/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
